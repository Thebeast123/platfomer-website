var tX = 0, //translateX value
    tW = -1, //initial index of widths array
    nW = 0, //total width of all the nav items(calculated using loop)
    widths = [], //array to store widths of all the nav items
    navBar = document.querySelector(".nav-wrapper > nav > .nav-in"); //internal nav container to be translated on x axis

//helper function that returns a the array of selector passed as the argument
function $$(selector) {
    var els = document.querySelectorAll(selector);
    return [].slice.call(els);
}
//assigning widths of nav items to widths[], and calculating total width of all the nav items
for (var i = 0; i < navBar.children.length; i++) {
    var item = navBar.children[i];
    nW += item.clientWidth;
    widths[i] = item.clientWidth;

}

//updating widths of nav items to widths[] and total width of all the nav items

window.addEventListener("resize", function () {
    for (var i = 0; i < navBar.children.length; i++) {
        var item = navBar.children[i];
        nW += item.clientWidth;
        widths[i] = item.clientWidth;

    }
});

//getting all the buttons having .btn class inside .new wrapper

$$(".nav-wrapper .btn").forEach(function (el) {
    el.addEventListener("click", function () {
        //for .btn-next
        if (this.classList.contains("btn-next")) {
            //increment tW by 1 to be used as indes of widths[]
            tW++;
            //decreasing the value od tX by width of the nav item(widths[tW]) every time .btn-next is clicked
            tX -= widths[tW];
            //(see first media query, .spacer was removed by setting dusplay to none, but navBar.children.length still counts what it did when .spacer was not set display: none) If index of widths[] becomes equal or more than the visible nav items, set translate value(tX) to -1* sum of widths ofnav items which is where the navBar is currently translated to. prevent tW from incrementing further
            if (tW >= navBar.children.length - 2) {
                tX = nW * -1;
                tW = navBar.children.length - 2;
            }
            //prevent tW from incrementing further by disabling cursor on .btn-next
            if (tW >= navBar.children.length - 3) {
                this.style.pointerEvents = "none";
            }
            //apply the translattion value
            navBar.style.transform = "translateX(" + tX + "px)";
        }

        //for previous button
        if (this.classList.contains("btn-prev")) {
            //set pointer-events of .btn-next back to auto
            document.querySelector(".btn-next").style.pointerEvents = "auto";
            //if .btn-next has been clicked one | >1 time i.e. when tX<0 (negative)
            if (tX < 0) {
                //decrement tW: the index
                tW--;
                //update tX
                tX += widths[tW];
                //apply tX value
                navBar.style.transform = "translateX(" + tX + "px)";
            }
            //if index(tW) reaches 0
            if (tW == 0) {
                //reset tX
                tX = 0;
                //apply tX value
                navBar.style.transform = "translateX(" + tX + "px)";
                //reset initial value of index
                tW = -1;
            }
        }
    }, false);
});